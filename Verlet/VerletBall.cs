﻿using System.Drawing;

namespace GXPEngine.Verlet
{
    public class VerletBall : Canvas
    {
        public bool Pinned
        {
            get => _pinned;
            set => _pinned = value;
        }

        public Vec2 OldPosition
        {
            get => _oldPosition;
            set => _oldPosition = value;
        }

        public Vec2 Position
        {
            get => _position;
            set => _position = value;
        }

        public int Radius
        {
            get { return width / 2; }
            set { width = height = 2 * value; }
        }

        public Vec2 Velocity
        {
            get { return _position.Clone().Subtract(_oldPosition); }
            set { _oldPosition = _position.Clone().Subtract(value); }
        }

        public AnimationSprite Drawing
        {
            get => _drawing;
            set => _drawing = value;
        }

        public Vec2 Gravity => _gravity;

        private Vec2 _gravity = new Vec2(0, 0.65f);

        private AnimationSprite _drawing;
        private Vec2 _oldPosition;
        private Vec2 _position;
        private const float friction = 0.9f; 

        private bool _pinned;

        public VerletBall(Vec2 position, int radius) : base(radius * 2, radius * 2)
        {
            _position = position;
            _oldPosition = position?.Clone();
            SetOrigin(width / 2f, height / 2f);
            draw();
            visible = false;
        }

        public void Update()
        {
            if (!_pinned)
            {
                Step();
                UpdatePosition();
            }
        }

        private void draw()
        {
            if (_drawing == null)
            {
                graphics.FillEllipse(Brushes.DarkRed, 0, 0, width, height);
            }
            else
            {
                AddChild(_drawing);
            }
        }

        public void Step()
        {
            if (_pinned) return;
            
            Vec2 implicitVelocity = Velocity;
            implicitVelocity.Add(_gravity);
            _oldPosition = _position.Clone();
            _position.Add(implicitVelocity);
            Velocity = Velocity.Scale(friction);
            
            checkBoundaries();
        }
        
        public int GetFloor(Vec2 pos)
        {
            return MyGame.instance.GetMapManager().CurrentMap.GetIDAt(1, (int) pos.x / 32, (int) pos.y / 32);
        }

        public void UpdatePosition()
        {
            if (_pinned) return;
            x = _position.x;
            y = _position.y;
        }

        private void checkBoundaries()
        {
            Vec2 velocity = Velocity;
            Vec2 position = _position;

            Level level = MyGame.instance.GetMapManager().CurrentMap;

            int width = level.Map.width *
                        level.Map.tileWidth;
            int height = level.Map.height * level.Map.tileHeight;


            bool changed = false;

            if (position.x - Radius < 0)
            {
                position.x = Radius;
                velocity.x *= -friction;
                changed = true;
            }
            else if (position.x + Radius > width)
            {
                position.x = width - Radius;
                velocity.x *= -friction;
                changed = true;
            }

            if (position.y - Radius < 0)
            {
                position.y = Radius;
                velocity.y *= -friction;
                changed = true;
            }
            else if (position.y + Radius > height)
            {
                position.y = height - Radius;
                velocity.y *= -friction;
                changed = true;
            }

            if (changed)
            {
                _position = position;
                Velocity = velocity;
            }
        }
    }
}