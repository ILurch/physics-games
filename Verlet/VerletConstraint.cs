﻿using System;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;

namespace GXPEngine.Verlet
{
    public class VerletConstraint
    {
        public VerletBall Start => _start;
        public VerletBall End
        {
            get => _end;
            set => _end = value;
        }

        public Vec2 Mid
        {
            get
            {
                Vec2 offset = End.Position.Clone().Subtract(Start.Position);
                return offset.Add(Start.Position);
            }   
        }

        public LineSegment Line => _line;
        
        private VerletBall _start;
        private VerletBall _end;
        private float _distance;

        private LineSegment _line;

        private Sprite _sprite;

        private static int count;
        
        public VerletConstraint(VerletBall start, VerletBall end)
        {
            count++;
           // Console.WriteLine(count);
            _start = start;
            _end = end;
            _distance = _start.Position.DistanceSquared(_end.Position);
            _line = new LineSegment(_start.Position, _end.Position, pLineWidth:5);
            start.parent.AddChild(_line);
        }
        
        public void Step()
        {
            handleMovement();
        }
        
        public void Destroy()
        {
            MyGame.instance.getVerletManager().Remove(this);
        }
        
        private void handleMovement()
        {
            Vec2 direction = _end.Position.Clone().Subtract(_start.Position);
            float distance = direction.Length();
            float seperation = _distance - distance;
            Vec2 grow = direction.Clone().Normalize().Scale(seperation / 2);
            
            //Console.WriteLine($"Dist: {distance}, Grow: {grow}");

            if (!_start.Pinned)
            {
                _start.Position.Subtract(grow);
                if (_end.Pinned)
                    _start.Position.Subtract(grow);
            }
            if (!_end.Pinned)
            {
                _end.Position.Add(grow);
                if (_start.Pinned)
                    _end.Position.Add(grow);
            }
        }

        /// <summary>
        /// TODO
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="radius"></param>
        /// <returns></returns>
        public bool checkColision(Vec2 pos, float radius)
        {
            float dist = pos.DistanceSquared(Mid);
            return dist < radius + (_distance / 2);
        }
    }
}