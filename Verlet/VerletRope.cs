﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Xml;

namespace GXPEngine.Verlet
{
    public class VerletRope
    {
        public int ElementLength => _elementLength;
        public List<VerletConstraint> Elements => _elements;

        public float TotalLength => (Elements.Count * ElementLength);

        private Player.StringType _type;
        
        public float TotalVelocity
        {
            get
            {
                float velo = 0;
                foreach (VerletConstraint constraint in _elements)
                {
                    velo += constraint.End.Velocity.Length();
                    if (constraint.Equals(_elements[0]))
                        velo += constraint.Start.Velocity.Length();
                }
                return velo;
            }
        }

        public Vec2 Center
        {
            get
            {
                if (_elements.Count > 0)
                {
                    int amount = _elements.Count + 1;
                    int index = amount / 2;
                    if (index < _elements.Count)
                    {
                        return _elements[index].Start.Position;
                    }
                }

                return null;
            }
        }

        private List<VerletConstraint> _elements = new List<VerletConstraint>();
        private readonly int _elementLength;

        private Vec2 _startPos;

        public VerletRope(Vec2 startPos, int elementLength)
        {
            _elementLength = elementLength;
            _startPos = startPos;
        }

        private int elementsCount;
        private int oldElementsCount;

        public int NewElements()
        {
            elementsCount = _elements.Count;
            int output = elementsCount - oldElementsCount;
            oldElementsCount = elementsCount;
            return output;
        }

        public void Step()
        {
            if (TotalVelocity < 0.5f)
            {
                SetPinned();
            }
        }

        public VerletConstraint checkColision(Vec2 pos, float radius)
        {
            foreach (VerletConstraint constraint in _elements)
            {
                if (constraint.checkColision(pos, radius))
                {
                    return constraint;
                }
            }

            return null;
        }

        public void SetPinned()
        {
            foreach (VerletConstraint constraint in Elements)
            {
                constraint.Start.Velocity.SetXY(0, 0);
                constraint.End.Velocity.SetXY(0, 0);
                constraint.Start.Pinned = true;
                constraint.End.Pinned = true;
            }
        }

        public VerletConstraint AddElement(GameObject parent, Vec2 direction, Player.StringType type)
        {
            VerletConstraint above = _elements.Count > 0 ? _elements[_elements.Count - 1] : null;

            VerletBall start = above != null ? above.End : new VerletBall(_startPos.Clone(), 3);
            VerletBall end = new VerletBall(start.Position.Clone().Add(direction.Normalize().Scale(_elementLength)), 3);

            if (!parent.HasChild(start))
                parent.AddChild(start);
            if (!parent.HasChild(end))
                parent.AddChild(end);

            VerletConstraint element = new VerletConstraint(start, end);
            _type = type;
            element.Line.lineWidth = _type.Thickness;
            element.Line.color = _type.Color;
            element.Start.Pinned = true;
            element.End.Pinned = true;
            _elements.Add(element);

            MyGame.instance.getVerletManager().Add(element);

            return element;
        }

        public void Release(VerletConstraint hit)
        {
            foreach (VerletConstraint constraint in Elements)
            {
                constraint.Line.color = _type.Color;
                constraint.Line.lineWidth = _type.Thickness;
                if (constraint.Equals(Elements[0]))
                {
                    constraint.End.Pinned = false;
                }
                else if (constraint.Equals(Elements[Elements.Count - 1]))
                {
                    constraint.Start.Pinned = false;
                    if (hit != null)
                    {
                        constraint.End.Pinned = false;
                        if (constraint.End.Position.DistanceNotSquared(hit.Start.Position) <
                            constraint.End.Position.DistanceNotSquared(hit.End.Position))
                        {
                            VerletConstraint constr = new VerletConstraint(hit.End, constraint.Start);
                            constr.Line.lineWidth = _type.Thickness;
                            constr.Line.color = _type.Color;
                            MyGame.instance.getVerletManager().Add(constr);
                        }
                        else
                        {
                            VerletConstraint constr = new VerletConstraint(hit.End, constraint.End);
                            constr.Line.lineWidth = _type.Thickness;
                            constr.Line.color = _type.Color;
                            MyGame.instance.getVerletManager().Add(constr);

                        }
                        // VerletConstraint ba = new VerletConstraint(constraint.End, hitTest.End);
                        // MyGame.instance.getVerletManager().Add(ba);
                    }
                }
                else
                {
                    constraint.Start.Pinned = false;
                    constraint.End.Pinned = false;
                }
            }
        }
    }
}