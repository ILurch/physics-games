﻿using System.Collections.Generic;

namespace GXPEngine.Verlet
{
    public class VerletManager
    {
        public List<VerletRope> Ropes => _ropes;
        
        private List<VerletConstraint> _contraints = new List<VerletConstraint>();
        private List<VerletRope> _ropes = new List<VerletRope>();

        public void Update()
        {
            foreach (VerletRope rope in _ropes)
            {
                rope.Step();
            }
            foreach (VerletConstraint contraint in _contraints)
            {
                contraint.Step();
            }
        }

        public void Add(VerletConstraint constraint)
        {
            _contraints.Add(constraint);
        }

        public void Remove(VerletConstraint constraint)
        {
            _contraints.Remove(constraint);
        }
        public void Add(VerletRope rope)
        {
            _ropes.Add(rope);
        }
    }
}