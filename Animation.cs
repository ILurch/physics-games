﻿using System;

namespace GXPEngine
{
    public class Animation
    {
        public int Count => _custumOrder[_index];

        public int[] Order => _custumOrder;

        private int[] _custumOrder;

        private int _index;

        private int _count;
        private readonly int _speed;

        private int _delayCount;
        private int _delay;

        private readonly int _delayMin;
        private readonly int _delayMax;

        private bool _loop;

        private bool _running;

        public Animation(int[] custumOrder, int speed, int delayMin = 0, int delayMax = 0, bool loop = true)
        {
            _custumOrder = custumOrder;
            _delayMax = delayMax;
            _delayMin = delayMin;
            _speed = speed;
            _loop = true;
            _running = true;
        }

        public void Stop()
        {
            _count = 0;
            _index = 0;
            _delayCount = 0;
            if (!_loop)
            {
                _running = false;
                _delay = 0;
            }
            else
            {
                _delay = Utils.Random(_delayMin, _delayMax);
            }
        }

        public void Start()
        {
            _running = true;
        }

        public void Pause()
        {
            _running = false;
        }

        /// <summary>
        /// Call Update() in class that uses Animation
        /// </summary>
        public void Update()
        {
            // delay between each animation
            if (_delayCount == _delay)
            {
                if (_running)
                {
                    // delay between each frame
                    if (_count == _speed)
                    {
                        _index++;
                        _count = 0;
                        if (_index >= _custumOrder.Length)
                        {
                            Stop();
                        }
                    }
                    else
                    {
                        _count++;
                    }
                }
            }
            else
            {
                _delayCount++;
            }
        }
    }
}