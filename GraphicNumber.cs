﻿using System;

namespace GXPEngine
{
    public class GraphicNumber
    {
        private AnimationSprite[] _arr;
        private Vec2 _position;
        private GameObject _parent;
        private int _margin;
        private int _scale;

        public GraphicNumber(GameObject parent, Vec2 pos, float scale = 1, int margin = -45, int i = 0)
        {
            _position = pos;
            char[] toDisplay = GetCharArr(i);
            _arr = new AnimationSprite[toDisplay.Length];
            _parent = parent;
            _margin = margin;
            Update(i);
        }

        public static char[] GetCharArr(int i)
        {
            return ("" + i).ToCharArray();
        }

        private void Clear()
        {
            if (_arr == null) return;
            foreach (AnimationSprite spr in _arr)
            {
                if (spr != null)
                    spr.Destroy();
            }
        }

        public void Update(int i)
        {
            Clear();
            char[] toDisplay = GetCharArr(i);
            _arr = new AnimationSprite[toDisplay.Length];
            for (int j = 0; j < toDisplay.Length; j++)
            {
                _arr[j] = new AnimationSprite("HUD_numbers.png", 10, 1);
               // _arr[j].SetScaleXY(_scale);
                _arr[j].SetFrame(int.Parse(toDisplay[j].ToString()));
                _parent.AddChild(_arr[j]);
                
                float x = _position.x + ((_arr[j].width + _margin) * j);
                float y = _position.y - (_arr[j].height / 2f);
                _arr[j].SetXY(x, y);
            }
        }
    }
}