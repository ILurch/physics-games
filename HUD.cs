﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace GXPEngine
{
    public class HUD : Canvas
    {
        public GraphicNumber ScoreNumber => _scoreNumber;
        public GraphicNumber WaveNumber => _waveNumber;

        private AnimationSprite _gooBar;
        private GraphicNumber _waveNumber;
        private GraphicNumber _scoreNumber;
        private Sprite _hud;

        private AnimationSprite _stringType;

        public HUD() : base(1920, 121)
        {
            _hud = new Sprite("HUD.png");
            AddChild(_hud);

            _gooBar = new AnimationSprite("goo_bar.png", 3, 7);
            _gooBar.SetScaleXY(0.8f);
            _gooBar.x = 100;
            _gooBar.y = 10;
            AddChild(_gooBar);

            _waveNumber = new GraphicNumber(this, new Vec2(_hud.width / 2 + 50, 58),  scale: 1.5f);
            _scoreNumber = new GraphicNumber(this, new Vec2(1748, 60));

            _stringType = new AnimationSprite("hud_buttons_512x128.png", 2, 1);
            _stringType.SetXY(1210, 0);
            AddChild(_stringType);
        }

        public override int height
        {
            get { return _hud.height; }
        }

        public void SetGoo(int goo, int maxGoo)
        {
            float percentage = goo / (float) maxGoo;
            int frame = (int) (_gooBar.frameCount * (1 - percentage));
            if (frame > 19) frame = 19;
            _gooBar.SetFrame(frame);
        }

        public void SetWaveCount(int waveCount)
        {
            _waveNumber.Update(waveCount);
        }

        public void SetScore(int score)
        {
            _scoreNumber.Update(score);
        }

        public void setWebType(int type)
        {
            _stringType.SetFrame(type);
        }
    }
}