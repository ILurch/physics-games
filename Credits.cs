﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GXPEngine
{
    class Credits : Sprite
    {
            private MainMenu mainMenu = new MainMenu();

            public Credits(MainMenu pMainmenu) : base("credits_bg.png")
            {
                mainMenu = pMainmenu;
                //SetScaleXY(0.5F,0.5F);
            }

            public void Update()
            {
            if(Input.GetKeyDown(Key.SPACE))
                {
                mainMenu._hover.Play();
                Destroy();
                }
            }
        }
    }
