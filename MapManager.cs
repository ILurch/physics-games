﻿using System;
using GXPEngine.TMXParse;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Xml.Serialization;
using GXPEngine.Managers;
using GXPEngine.WaveParse;

namespace GXPEngine
{
    public class MapManager : GameObject
    {
        public Level CurrentMap => _currentLevel;

        private List<Map> _maps;
        private List<Waves> _waves;
        private List<Bitmap> _spawnAreas;

        private List<Sprite> _backgrounds = new List<Sprite>();
        
        private Level _currentLevel;
        

        private WaveManager _waveManager;
        
        public MapManager()
        {
            _maps = LoadMaps();
            _waves = LoadWaves();
            _spawnAreas = LoadSpawns();
        }


        public Level InitializeLevel(GameObject obj, int levelNumber)
        {
            if (_currentLevel != null && obj.HasChild(_currentLevel))
            {
                obj.RemoveChild(_currentLevel);
            }
            
            _backgrounds.Add(new Sprite("Level_Background1.png"));
            _backgrounds.Add(new Sprite("Level_Background2.png"));
            obj.AddChild(_backgrounds[levelNumber]);
            
            _currentLevel = new Level(_maps[levelNumber]);
            obj.AddChild(_currentLevel);
           // Console.WriteLine("Spawn Areas: " + _spawnAreas.Count);
            _waveManager = new WaveManager(obj, _waves[levelNumber],_spawnAreas[levelNumber]);
            return _currentLevel;
        }

        void InitializeBackground(int levelnuber)
        {
            
        }

        public WaveManager GetWaveManager()
        {
            return _waveManager;
        }

        public List<Map> GetMaps()
        {
            return _maps;
        }

        public List<Waves> GetWaves()
        {
            return _waves;
        }

        public List<Bitmap> GetSpawns()
        {
            return _spawnAreas;
        }

        private static List<Map> LoadMaps()
        {
            IEnumerable<string> files = Directory.GetFiles("rooms/").Where(s => s.EndsWith(".tmx"));
            List<Map> maps = new List<Map>();
            foreach (string file in files)
            {
                Map map = TMXParser.ParseTMXFile(file);
                maps.Add(map);
            }
            return maps;
        }

        private static List<Waves> LoadWaves()
        {
            IEnumerable<string> files = Directory.GetFiles("waves/").Where(s => s.EndsWith(".xml"));
            List<Waves> wavesList = new List<Waves>();
            foreach (string file in files)
            {
                Waves waves = WaveParser.ParseWaveFile(file);
                wavesList.Add(waves);
            }
            return wavesList;
        }

        private static List<Bitmap> LoadSpawns()
        {
            IEnumerable<string> files = Directory.GetFiles("spawns/").Where(s => s.EndsWith(".png"));
            List<Bitmap> spawnList = new List<Bitmap>();
            foreach (string file in files)
            {
                Bitmap spawn = new Bitmap(file);
                spawnList.Add(spawn);
            }
            return spawnList;
            
        }

        void Update()
        {
            if(_waveManager != null)
            _waveManager.Step();
        }
    }
}