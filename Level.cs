﻿using System;
using System.Collections.Generic;
using GXPEngine.Managers;
using GXPEngine.TMXParse;

namespace GXPEngine
{
    public class Level : TMXLevel
    {
        /*
         * Used for managing the current game level
         * Controls where player spawns
         * Controls where the enemies are spawned
         */

        public Level(Map map) : base(map)
        {
            generateSpriteLayers();
        }

        void Update()
        {
        }

        public AnimationSprite GetSpriteAt(int layerIndex, int x, int y)
        {
            try
            {
                AnimationSprite[,] sprArr = _layers[layerIndex];
                return sprArr[x, y];
            }
            catch (IndexOutOfRangeException ex)
            {
                return null;
            }
        }

        public int GetIDAt(int layerIndex, int x, int y)
        {
            AnimationSprite spr = GetSpriteAt(layerIndex, x, y);
            if (spr == null) return 0;
            return spr.currentFrame;
        }

        public List<AnimationSprite> GetTilesAround(int layer, Vec2 position, int size)
        {
            List<AnimationSprite> sprites = new List<AnimationSprite>();
            for (int x = 0; x < size; x++)
            {
                for (int y = 0; y < size; y++)
                {
                    int xP = (int) (position.x - (size / 2f) + x) / 32;
                    int yP = (int) (position.y - (size / 2f) + y) / 32;
                    sprites.Add(GetSpriteAt(layer, xP, yP));
                }
            }
            return sprites;
        }
    }
}