﻿namespace GXPEngine
{
    public class Popup : Canvas
    {
        private Animation _fadeInAnim = new Animation(new[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 10}, 4, 200, 250);
        private Animation _fadeOutAnim = new Animation(new[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 10}, 4, 200, 250);

        private bool _running;
        private int _stay;

        private bool _fadeIn = true;
        private bool _fadeOut = false;

        private AnimationSprite _graphics;
        
        public Popup(AnimationSprite graphics) : base(graphics.width, graphics.height)
        {
            _graphics = graphics;
            AddChild(graphics);
        }

        public void Update()
        {
            if (_fadeIn)
            {
                _fadeInAnim.Update();
            _graphics.SetFrame(_fadeInAnim.Count);
            } else if (_fadeOut)
            {
                _fadeOutAnim.Update();
                _graphics.SetFrame(_fadeOutAnim.Count);
            }
        }

        public void Start(int stay)
        {
            if (!_running)
            {
                _stay = stay;
                _running = true;
            }
        }
    }
}