﻿using System;

namespace GXPEngine
{
    public class Cursor : Sprite
    {
        private Vec2 _position;
        
        public Cursor() : base("Cursor.png")
        {
            _position = new Vec2(0,0);
        }

        void Update()
        {
            _position.SetXY(Input.mouseX - MyGame.instance.GetScreen().x,
                Input.mouseY - MyGame.instance.GetScreen().y);
            
            x = _position.x;
            y = _position.y;
        }
    }
}