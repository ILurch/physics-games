﻿namespace GXPEngine
{
    public class GameOver : Sprite
    {

        public GameOver() : base("gameover.png")
        {
            SetOrigin(width / 2f, height / 2f);
        }

        public void Show()
        {
            visible = true;
        }

        public void Hide()
        {
            visible = false;
        }
    }
}