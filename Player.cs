﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Security.Policy;
using System.Threading;
using GXPEngine.Physics;
using GXPEngine.TMXParse;
using GXPEngine.Verlet;
using GXPEngine.WaveParse;

namespace GXPEngine
{
    public class Player : AnimationSprite
    {
        public enum PlayerState
        {
            IDLE,
            MOVING,
            FALLING,
            SHOOTING,
            DIEING,
            EATING
        }

        public struct StringType
        {
            private static int count;
            public uint Thickness => _thickness;
            public uint Color => _color;
            public int ID => _id;

            private uint _thickness;
            private uint _color;
            private int _id;

            public StringType(uint thickness, uint color)
            {
                _id = count;
                count++;
                _thickness = thickness;
                _color = color;
            }
        }

        public static readonly StringType THICK = new StringType(5, MyGame.ToUint(Color.BlanchedAlmond));
        public static readonly StringType THIN = new StringType(2, MyGame.ToUint(Color.LemonChiffon));

        public int Score => _score;

        private Dictionary<PlayerState, Animation> _animations = new Dictionary<PlayerState, Animation>();

        private int _radius;
        private int _eatCounter;

        private const float _friction = 0.85f;
        private const float _speed = 0.55f;
        private float _enemyDetectionRange;

        private PhysicNode _physic;

        private PlayerState _oldState = PlayerState.IDLE;
        private PlayerState _state = PlayerState.IDLE;
        private StringType _type = THICK;

        private SoundChannel currentWalking;

        private List<RopeCreator> _ropeCreators = new List<RopeCreator>();

        private Animation currentAnimation;

        public int totalPieces;
        private int oldTotalPieces;

        private int _maxGoo = 800;
        private int _currentGoo;
        private int[] _gooPerEnemy = {5, 10};

        private int _score;

        private VerletConstraint _hitWeb;
        
        private const int Regendelay = 1;
        private int regenDelayCount = 0;

        public Player(Vec2 startPosition) : base("Sprite_Spider.png", 16, 2)
        {
            SetOrigin(width / 2f, height / 2f);
            _radius = width / 2 - 5;
            initAnims();
            currentAnimation = _animations[_state];
            _physic = new PhysicNode(startPosition, width / 2);
            _physic.visible = false;
            _currentGoo = _maxGoo / 2;
            MyGame.instance.GetMapManager().CurrentMap.AddChild(_physic);
            _enemyDetectionRange = 30;
        }

        private void initAnims()
        {
            int[] movingArr = new int[] {11, 10, 9, 8, 7, 8, 9, 10};
            _animations.Add(PlayerState.MOVING, new Animation(movingArr, 4));
            int[] idleArr = new int[] {6, 5, 4, 3, 4, 5};
            _animations.Add(PlayerState.IDLE, new Animation(idleArr, 6, 75, 125));
            int[] shooting = new int[] {0};
            _animations.Add(PlayerState.SHOOTING, new Animation(shooting, 0));
            int[] dieing = new int[] {26, 25, 24, 23, 22};
            _animations.Add(PlayerState.DIEING, new Animation(dieing, 4, loop: false));
            int[] eating = new int[] {16, 17, 18, 19, 20, 21, 20, 19, 18, 17};
            _animations.Add(PlayerState.EATING, new Animation(eating, 4, loop: false));
            int[] falling = {15, 14, 13, 12, 13, 14};
            _animations.Add(PlayerState.FALLING, new Animation(falling, 4));
        }

        void Update()
        {
            _oldState = _state;
            updateAnimation();
            checkColision();

            SetXY(_physic.Pos.x, _physic.Pos.y);
            _physic.Velocity.Scale(_friction);

            controlStringType();
            controlShooting();
            //updateCurrentLine();
            updateRopeCreators();
            rotateToMouse();
            updateEating();
            CheckMaxGoo();
            CheckNewPieces();
            GooRegen();
            //Console.WriteLine("Current Goo: " + (_currentGoo/_maxGoo) + "%");
            MyGame.instance.GetScreen().HUD.SetGoo(_currentGoo, _maxGoo);
        }


        void GooRegen()
        {
            if (regenDelayCount > Regendelay)
            {
                _currentGoo++;
                CheckMaxGoo();
                MyGame.instance.GetScreen().HUD.SetGoo(_currentGoo, _maxGoo);
                regenDelayCount = 0;
            }
            else
            {
                regenDelayCount++;
            }
        }

        private void CheckNewPieces()
        {
            if (totalPieces > oldTotalPieces)
            {
                if (_type.Equals(THICK))
                {
//Thick Line
                    _currentGoo -= 10;
                }
                else
                {
//Thin Line
                    _currentGoo -= 5;
                }
            }
            oldTotalPieces = totalPieces;
        }

        private void CheckMaxGoo()
        {
            if (_currentGoo > _maxGoo)
                _currentGoo = _maxGoo;
        }

        private void updateEating()
        {
            if (_state == PlayerState.EATING)
            {
                _eatCounter++;
                if (_eatCounter == 40)
                {
                    _state = PlayerState.IDLE;
                    _eatCounter = 0;
                }
            }
        }

        private void updateAnimation()
        {
            if (_oldState != _state)
            {
                currentAnimation.Stop();
                currentAnimation = _animations[_state];
            }
            else
            {
                currentAnimation.Update();
                SetFrame(currentAnimation.Count);
            }
        }

        private void checkColision()
        {
            _hitWeb = null;
            bool hitWeb = false;
            foreach (VerletRope rope in MyGame.instance.getVerletManager().Ropes)
            {
                if ((_hitWeb = rope.checkColision(_physic.Pos, _physic.width / 3f)) != null)
                {
                    hitWeb = true;
                    break;
                }
            }
            bool hitTile = (GetFloor(_physic.Pos) != 0 || GetFloor(_physic.Pos, 2) != 0);
            if (!hitTile && !hitWeb)
            {
                _physic.UseGravity = true;
            }
            else
            {
                _physic.UseGravity = false;
                controlMovement();
            }
            //
            EnemyView view = CheckEnemyCollision();
            if (view != null && view.GetState() == EnemyView.EnemyState.CAUGHT)
            {
                //Console.WriteLine("Eating");
                _state = PlayerState.EATING;
                _currentGoo += _gooPerEnemy[view.GetType()];
                _score++;
            }
        }

        private void rotateToMouse()
        {
            Vec2 mouseOffset = new Vec2(Input.mouseX - MyGame.instance.GetScreen().x - _physic.x,
                Input.mouseY - MyGame.instance.GetScreen().y - _physic.y - 121);
            rotation = mouseOffset.GetAngle(true);
        }


        private void controlStringType()
        {
            if (Input.GetKeyDown(Key.Q))
            {
                _type = THICK.Equals(_type) ? THIN : THICK;
                MyGame.instance.GetScreen().HUD.setWebType(_type.ID);
            }
        }

        private void updateRopeCreators()
        {
            foreach (RopeCreator creator in _ropeCreators.ToArray())
            {
                VerletConstraint ropeHitTest = creator.checkCollision();

                // if rope hits the wall or web the rope can be realeased from it's projectile and all elements will we updated
                if (ropeHitTest != null || (creator.LastElement != null &&
                                            0 != GetFloor(creator.LastElement.End.Position) &&
                                            GetFloor(creator.LastElement.Start.Position) == 0))
                {
                    creator.Rope.Release(ropeHitTest);
                    _ropeCreators.Remove(creator);
                    creator.Destroy();
                }
                creator.Update();
            }
        }

        /* private void updateCurrentLine()
         {
             if (_currentLine != null)
                 _currentLine.start.SetXY(_position.x, _position.y);
             if (Input.GetKeyDown(Key.SPACE))
                 _currentLine = null;
         }*/

        private void controlShooting()
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (_state != PlayerState.FALLING)
                {
                    if (_state != PlayerState.FALLING && _currentGoo > 1)
                    {
                        _state = PlayerState.SHOOTING;

                        Vec2 mousePos = new Vec2(Input.mouseX, Input.mouseY);
                        Vec2 offset = ((mousePos) - _physic.Pos);

                        PhysicNode node = new PhysicNode(_physic.Pos.Clone(), 3);
                        node.Velocity.Add(_physic.Velocity);
                        node.Velocity.Add(new Vec2(rotation, true).Scale(33));

                        parent.AddChild(node);

                        RopeCreator creator = new RopeCreator(_physic.Pos, node, _type, this);
                        _ropeCreators.Add(creator);

                        Sound sound = new Sound("mainMenuClick.ogg");
                        sound.Play();
                    }
                }
            }
        }

        public int GetFloor(Vec2 pos, int layer = 0)
        {
            return MyGame.instance.GetMapManager().CurrentMap.GetIDAt(layer, (int) pos.x / 32, (int) pos.y / 32);
        }

        public EnemyView CheckEnemyCollision()
        {
            List<EnemyView> enemies = MyGame.instance.GetMapManager().GetWaveManager().GetEnemies();
            if (enemies != null)
            {
                foreach (EnemyView enemy in enemies)
                {
                    if (!enemy.beingEaten && enemy.GetPosition().DistanceSquared(_physic.Pos) < _physic.width / 3f)
                    {
                        enemy.beingEaten = true;
                        return enemy;
                    }
                }
            }
            return null;
        }

        private void controlMovement()
        {
            if (_state != PlayerState.EATING)
            {
                if (Math.Abs(_physic.Velocity.x) < 0.05 && Math.Abs(_physic.Velocity.y) < 0.05)
                {
                    _state = PlayerState.IDLE;
                }
                else _state = PlayerState.FALLING;
                if (Input.GetKey(Key.W))
                {
                    _physic.Velocity.Add(new Vec2(rotation, true).Scale(_speed));
                    _state = PlayerState.MOVING;
                    if (currentWalking == null || !currentWalking.IsPlaying)
                    {
                        Sound sound = new Sound("walking1.ogg");
                        Sound sound1 = new Sound("walking2.ogg");
                        int i = Utils.Random(0, 1);
                        if (i == 0)
                        {
                            currentWalking = sound.Play();
                        }
                        else
                        {
                            sound1.Play();
                        }
                    }
                }
                else if (Input.GetKey(Key.S))
                {
                    _physic.Velocity.Add(new Vec2(rotation, true).Scale(-_speed));
                    _state = PlayerState.MOVING;
                    if (currentWalking == null || !currentWalking.IsPlaying)
                    {
                        Sound sound = new Sound("walking1.ogg");
                        Sound sound1 = new Sound("walking2.ogg");
                        int i = Utils.Random(0, 1);
                        if (i == 0)
                        {
                            currentWalking = sound.Play();
                        }
                        else
                        {
                            sound1.Play();
                        }
                    }
                }
                if (Input.GetKey(Key.A))
                {
                    rotation -= 4;
                }
                if (Input.GetKey(Key.D))
                {
                    rotation += 4;
                }
                _physic.Pos.Add(_physic.Velocity);
            }
        }

        public Vec2 GetPosition()
        {
            return _physic.Pos;
        }

        public int GetGoo()
        {
            return _currentGoo;
        }

        public int GetMaxGoo()
        {
            return _maxGoo;
        }
    }
}