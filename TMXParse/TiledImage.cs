﻿using System.Xml.Serialization;

namespace GXPEngine.TMXParse
{
    [XmlRootAttribute("image")]
    public class TiledImage
    {
        [XmlAttribute("source")]
        public string source;

        [XmlAttribute("width")]
        public int width;

        [XmlAttribute("height")]
        public int height;

        public override string ToString()
        {
            return $"Source: {source}, width:{width}, height:{height}";
        }
    }
}