﻿namespace GXPEngine.TMXParse
{
    public enum Rotation
    {
        DEGREE_0, DEGREE_90, DEGREE_180, DEGREE_270
    }

}