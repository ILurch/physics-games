﻿using System;
using System.Xml.Serialization;

namespace GXPEngine.TMXParse
{
    [XmlRootAttribute("map")]
    public class Map
    {
        [XmlAttribute("version")]
        public string version = "";
        
        [XmlAttribute("orientation")]
        public string orientation = "";
        
        [XmlAttribute("renderorder")]
        public string renderOrder = "";

        [XmlAttribute("tilewidth")]
        public int tileWidth = 0;

        [XmlAttribute("tileheight")]
        public int tileHeight = 0;

        [XmlAttribute("height")] 
        public int height = 0;

        [XmlAttribute("width")]
        public int width = 0;

        [XmlElement("tileset")] 
        public TileSet tileset;
       
        [XmlElement("layer")] 
        public Layer[] layers;

        [XmlElement("objectgroup")]
        public ObjectGroup[] objectGroups;

        public string file;

        public override string ToString()
        {
            string layerString = "";
            foreach (Layer layer in layers)
            {
                layerString += layer + "\n";
            }
            
            string objectGroupString = "";
            if (objectGroups != null)
            {
                foreach (ObjectGroup group in objectGroups)
                {
                    objectGroupString += group + "\n";
                }
            }

            return $"Version: {version}\nOrientation: {orientation}\nrenderorder: {renderOrder}\ntilewidth: {tileWidth}\n" +
                   $"tileheight {tileHeight}\nwidth: {width}\nheight: {height}\nTileSet: {tileset}" +
                   $"\n\nObjectlayers: \n{objectGroupString}\nLayers:\n{layerString}";
        }
    }
}