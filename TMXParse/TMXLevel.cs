﻿using System;
using System.Collections.Generic;
using System.Drawing;
using GXPEngine.TMXParse;
using Object = GXPEngine.TMXParse.Object;

namespace GXPEngine
{
    public class TMXLevel : GameObject
    {
        public Map Map => _map;
        
        protected readonly Map _map;
        protected List<AnimationSprite[,]> _layers;

        public TMXLevel(Map map)
        {
            _map = map;
            _layers = new List<AnimationSprite[,]>();
            generateSpriteLayers();
        }
        
        protected void generateSpriteLayers()
        {
            foreach (Layer layer in _map.layers)
            {
                AnimationSprite[,] sprites = new AnimationSprite[layer.width, layer.height];
                for (int x = 0; x < layer.data.getDecodedTileData().GetLength(0); x++)
                {
                    for (int y = 0; y < layer.data.getDecodedTileData().GetLength(1); y++)
                    {
                        int i = layer.data.getDecodedTileData()[x, y];
                        if (i == 0) continue;
                        AnimationSprite spr = new AnimationSprite( _map.tileset.image.source, _map.tileset.columns, _map.tileset.tileCount / _map.tileset.columns);
                        spr.SetFrame(i - 1);
                        spr.SetXY(x * _map.tileWidth, y * _map.tileHeight);
                        spr.rotation = ((int) layer.data.GetTileRotations()[x, y]) * 90;
                        AddChild(spr);
                        sprites[x, y] = spr;
                    }
                }
                _layers.Add(sprites);
            }
        }
        
        public Vec2 FindPlayerSpawn()
        {
            for (int i = 0; i < _map.objectGroups.Length; i++)
            {
                foreach (ObjectGroup group in _map.objectGroups)
                {
                    for (int j = 0; j < group.objects.Length; j++)
                    {
                        return new Vec2(group.objects[j].x,group.objects[j].y);
                    }
                }
            }
            return null;
        }
    }
}