﻿using System;
using System.Xml.Serialization;

namespace GXPEngine.TMXParse
{
    [XmlRootAttribute("tileset")]
    public class TileSet
    {
        [XmlAttribute("firstgid")]
        public int firstgid;

        [XmlAttribute("name")]
        public String name;

        [XmlAttribute("tilewidth")]
        public int tileWidth;

        [XmlAttribute("tileheight")]
        public int tileHeight;

        [XmlAttribute("tilecount")]
        public int tileCount;

        [XmlAttribute("columns")]
        public int columns;

        [XmlElement("image")] 
        public TiledImage image;

        public override string ToString()
        {
            return $"FirstGid: {firstgid}, Name: {name}, TileWidth: {tileWidth}, TileHeight: {tileHeight}, TileAmount: {tileCount}, Image: {image} ";
        }
    }
}