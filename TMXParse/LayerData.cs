﻿using System.Xml.Serialization;
using GXPEngine.TMXParse;

namespace GXPEngine
{
    [XmlRootAttribute("data")]
    public class LayerData
    {
        [XmlAttribute("encoding")] public string encoding;

        [XmlText] public string innerXML;

        private int[,] _tileMap;
        private Rotation[,] _rotationMap;

        /// <summary>
        /// Must be called before accessing getDecodedTileData() & getTileRotations() to avoid NullReferenceException
        /// </summary>
        public void GenerateTileArrays(int width, int height)
        {
            _tileMap = new int[width, height];
            _rotationMap = new Rotation[width, height];
            string[] tilesData = innerXML.Trim().Split(',');
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    long tileId = long.Parse(tilesData[x + y * width]);
                    if (tileId >= 3221225472)
                    {
                        _rotationMap[x, y] = Rotation.DEGREE_180;
                        _tileMap[x, y] = (int) (tileId - 3221225472);
                    }
                    else if (tileId >= 2684354560)
                    {
                        _rotationMap[x, y] = Rotation.DEGREE_90;
                        _tileMap[x, y] = (int) (tileId - 2684354560);
                    }
                    else if (tileId >= 1610612736)
                    {
                        _rotationMap[x, y] = Rotation.DEGREE_270;
                        _tileMap[x, y] = (int) (tileId - 1610612736);
                    }
                    else
                    {
                        _rotationMap[x, y] = Rotation.DEGREE_0;
                        _tileMap[x, y] = (int) tileId;
                    }
                }
            }
        }

        public int[,] getDecodedTileData()
        {
            return _tileMap;
        }

        public Rotation[,] GetTileRotations()
        {
            return _rotationMap;
        }


        public override string ToString()
        {
            string dataOutput = "";
            for (int x = 0; x < _tileMap.GetLength(0); x++)
            {
                string line = "";
                for (int y = 0; y < _tileMap.GetLength(1); y++)
                {
                    line += _tileMap[x, y] + ":";
                    line += _rotationMap[x, y].ToString().Substring(7) + ",";
                }
                dataOutput += line + "\n";
            }
            return dataOutput;
        }
    }
}