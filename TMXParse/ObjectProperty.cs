﻿using System;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Xml.Serialization;

namespace GXPEngine.TMXParse
{
    [XmlRootAttribute("property")]
    public class ObjectProperty
    {
        [XmlAttribute("name")] public string name;

        [XmlAttribute("type")] public string type = "string";

        [XmlAttribute("value")] public string value;
        
        public System.Object getValue()
        {
            switch (type)
            {
                case("string"): return value;
                case("int"): return int.Parse(value);
                case("bool"): return bool.Parse(value);
                case("color"): return ColorTranslator.FromHtml(value);
                case("file"): return value;
                case("float"): return float.Parse(value);
            }
            return null;
        }

        public override string ToString()
        {
            return $"{name}: {getValue().ToString().Replace("[",  "").Replace("]", "")}[{type}]";
        }
    }
}