﻿using System.IO;
using System.Xml.Serialization;

namespace GXPEngine.TMXParse
{
    public class TMXParser
    {
        private TMXParser()
        {
        }

        public static Map ParseTMXFile(string file)
        {
            XmlSerializer _serializer = new XmlSerializer(typeof(Map));

            TextReader reader = new StreamReader(file);
            Map map = _serializer.Deserialize(reader) as Map;
            map.file = file;
            reader.Close();
            foreach (Layer layer in map.layers)
            {
                layer.data.GenerateTileArrays(layer.width, layer.height);
            }
            return map;
        }
    }
}