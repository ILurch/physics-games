﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace GXPEngine.TMXParse
{
    [XmlRootAttribute("object")]
    public class TiledObject
    {
        [XmlAttribute("id")]
        public int id;

        [XmlAttribute("type")] 
        public string type;
        
        [XmlAttribute("x")]
        public int x;
        
        [XmlAttribute("y")]
        public int y;
        
        [XmlAttribute("width")]
        public int width;
        
        [XmlAttribute("height")]
        public int height;

        [XmlAttribute("rotation")]
        public int rotation;

        [XmlElement("properties")]
        public ObjectProperties properties;

        [XmlElement("polygon")] 
        public Polygon polygon;
        
        public override string ToString()
        {
            string propertyString = "";
            if (properties != null)
            {
                foreach (ObjectProperty prop in properties.property)
                {
                    propertyString += ", " + prop.ToString();
                    Console.WriteLine(prop);
                }
            }
            string polyString = "";
            if (polygon != null)
            {
                polyString += polygon;
            }
            return $"id:{id}, type: {type}, x: {x}, y: {y}, width: {width}, height: {height}, rotation: {rotation}{propertyString}, PolygonPoints: {polyString}";
        }
    }
}