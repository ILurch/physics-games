﻿using System.Xml.Serialization;

namespace GXPEngine.TMXParse
{
    [XmlRootAttribute("objectgroup")]
    public class ObjectGroup
    {
        [XmlAttribute("name")] 
        public string name;

        [XmlElement("object")]
        public TiledObject[] objects;

        public override string ToString()
        {
            string output =  name + ":" + "\n";
            foreach (TiledObject obj in objects)
            {
                output += obj;
                if (objects.Length > 0 && !objects[objects.Length - 1].Equals(obj))
                {
                    output += "\n";
                } 
            }
            return output;
        }
    }
}