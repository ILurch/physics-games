﻿using System;
using System.Globalization;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Xml.Serialization;

namespace GXPEngine.TMXParse
{
    [XmlRootAttribute("layer")]
    public class Layer
    {
        
        [XmlAttribute("name")] 
        public string name;

        [XmlAttribute("width")] 
        public int width = 0;

        [XmlAttribute("height")] 
        public int height = 0;

        [XmlElement("data")] 
        public LayerData data;

        public override string ToString()
        {
            return $"{name}:\nwidth: {width}, height: {height}\n{data}";
        }
    }
}