﻿using System;
using System.Collections.Generic;

namespace GXPEngine.TMXParse
{
    public class Object : GameObject
    {
        public const int TYPE_SPAWN_P = 1;
        public const int TYPE_SPAWN_E = 2;
        
        public Dictionary<string,string> properties = new Dictionary<string, string>();

        private int _type;
        
        public Object(TiledObject tiledObject)
        {
            x = tiledObject.x;
            y = tiledObject.y;
            if (tiledObject.properties?.GetProperty("type") != null)
            {
                properties["type"] = tiledObject.properties.GetProperty("type").value;
                _type = int.Parse(tiledObject.properties.GetProperty("type").value);
            }
            if (tiledObject.properties?.GetProperty("name") != null)
                properties["name"] = tiledObject.properties.GetProperty("name").value;
        }

        public int GetObjectType()
        {
            if (_type != 0)
            {
                return _type;
            }
            return 0;
        }
    }
}