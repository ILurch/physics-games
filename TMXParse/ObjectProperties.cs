﻿using System.Xml.Serialization;

namespace GXPEngine.TMXParse
{
    [XmlRootAttribute("properties")]
    public class ObjectProperties
    {
        [XmlElement("property")]
        public ObjectProperty[] property;

        public ObjectProperty GetProperty(string name)
        {
            foreach (ObjectProperty p in property)
            {
                if (p.name.Equals(name)) return p;
            }
            return null;
        }
    }
}