﻿using System.Xml.Serialization;

namespace GXPEngine.TMXParse
{
    [XmlRootAttribute("polygon")]
    public class Polygon
    {
        [XmlAttribute("points")] 
        public string points;

        public override string ToString()
        {
            return points;
        }
    }
}