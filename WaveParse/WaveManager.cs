﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace GXPEngine.WaveParse
{
    public class WaveManager
    {
        public interface WaveListener
        {
            void onGameOver();
        }
        
        public int WaveCount => _waveNumber;
        public int Waves => _waves.Length;

        public Wave CurrentWave => _currentWave;

        public static int EnemyCount = 0;
        
        public delegate void OnGameOver();
        
        private Wave[] _waves;
        private Wave _currentWave;
        private Enemy[] _enemies;
        private GameObject _target;
        private Bitmap _spawnArea;

        private List<EnemyView> _enemyList = new List<EnemyView>();

        private Random rnd;

        private int count;
        private int _enemySplitCount;

        private int _waveNumber;
        private int _enemyWaveCount;
        private int _enemyCount;

        private bool _waveActive;
        private bool _wavePaused;

        private WaveListener _listener;
        
        public WaveManager(GameObject targetObject, Waves waves, Bitmap spawnArea)
        {
            rnd = new Random();
            _target = targetObject;
            _waves = waves.waves;
            _spawnArea = spawnArea;
            _waveNumber = 0;
            _currentWave = _waves[_waveNumber];
            InitializeWave();
        }

        public void InitializeWave()
        {
            if (_currentWave != null)
            {
                _enemies = _currentWave.enemies;
                count = 0;
                //Enables UpdateWave to do its thing
                _waveActive = true;
            }
        }

        void NextWave()
        {
           // Console.WriteLine("Next Wave");
            //Console.WriteLine("Wave Number: " + _waveNumber);
            //Console.WriteLine("Total Wave Numbers: " + _waves.Length);
            if (_waveNumber < _waves.Length - 1)
            {
                _waveNumber++;
                _currentWave = _waves[_waveNumber];
                InitializeWave();
                _enemyWaveCount = 0;
                EnemyCount = 0;
            }
            else
            {
                _listener.onGameOver();
                Console.WriteLine("Game Over!");
            }
        }

        void UpdateWave()
        {
            if (_currentWave != null)
            {
                if (_enemyWaveCount <= _enemies.Length -1)
                {
                    if (_enemyCount < _enemies[_enemyWaveCount].amount)
                    {
                        _enemySplitCount++;
                        if (_enemySplitCount % _enemies[_enemyWaveCount].split == 0)
                        {
                            _enemyCount++;
                            SpawnEnemy(RandomPosition(_spawnArea),_enemies[_enemyWaveCount].type);
                        }
                    }
                    else
                    {
                        _enemyWaveCount++;
                        if (_enemyWaveCount == _enemies.Length)
                        {
                            _waveActive = false;
                            Console.WriteLine("End of Wave");
                        }
                        else
                        {
                            _enemyCount = 0;
                            _wavePaused = true;
                            //Console.WriteLine("Waiting for Delay");
                            count = 0;
                        }
                    }
                }
            }
        }
        
        
        public void SpawnEnemy(Vec2 position, int type)
        {
            EnemyCount++;
            EnemyView enemy = new EnemyView(type,position);
            _enemyList.Add(enemy);
            _target.AddChild(enemy);
        }

        public List<EnemyView> GetEnemies()
        {
            return _enemyList;
        }

        private Vec2 RandomPosition(Bitmap spawnArea)
        {
            Vec2 randomPositionVec;
            do
            {
                randomPositionVec = new Vec2(RandomNumber(1, spawnArea.Width), RandomNumber(1, spawnArea.Height));
            } 
            while (CheckBitPosition(spawnArea ,randomPositionVec ) == false);
            return randomPositionVec;
        }

        private bool CheckBitPosition(Bitmap spawnArea, Vec2 position)
        {
            if (spawnArea.GetPixel((int) position.x, (int) position.y).R == 255)
                return true;
            return false;
        }

        private int RandomNumber(int min, int max)
        {
            return rnd.Next(min, max);
        }

        public void Step()
        {
            if (_currentWave != null && _waveActive)
            if (!_wavePaused)
            {
                UpdateWave();
            }
            else
            {
                count++;
                if (count == _enemies[_enemyWaveCount].delay * 60)
                    _wavePaused = false;
            }

            if (Input.GetKeyDown(Key.O))
            {
                if (!_waveActive)
                {
                    NextWave();
                }
            }
        }

        public void SetListener(WaveListener listener)
        {
            _listener = listener;
        }
    }
}