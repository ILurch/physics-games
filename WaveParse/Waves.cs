﻿using System;
using System.Xml.Serialization;

namespace GXPEngine
{
    [XmlRootAttribute("waves")]
    public class Waves
    {
        [XmlElement("wave")] public Wave[] waves;
        
        [XmlIgnore]
        public string file;

        public override string ToString()
        {
            string output = "";
            output += "Number of Waves: " + waves.Length + "\n";
            foreach (Wave wave in waves)
            {
                output += wave;
            }
            return output;
        }
    }
}