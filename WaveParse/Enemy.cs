﻿using System.Xml.Serialization;

namespace GXPEngine.WaveParse
{
    [XmlRoot("enemy")]
    public class Enemy
    {   
        [XmlAttribute("type")] public int type;
        [XmlAttribute("amount")] public int amount;
        [XmlAttribute("split")] public int split ;
        [XmlAttribute("delay")] public int delay;

        public override string ToString()
        {
            return $"Type({type}) - Amount({amount}) - Split({split}) - Delay({delay})";
        }
    }
}