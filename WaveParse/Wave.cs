﻿using System;
using System.Xml.Serialization;
using GXPEngine.WaveParse;

namespace GXPEngine
{
    [XmlRoot("wave")]
    public class Wave
    {
        [XmlAttribute("number")] public int number;
        [XmlElement("enemy")] public Enemy[] enemies;

        public override string ToString()
        {
            string output = "";
            output += "Wave: " + number;
            output += " Nr. of Enemies: " + enemies.Length + "\n";
            foreach (Enemy enemy in enemies)
            {
                output += enemy + "\n";
            }
            return output;
        }
    }
}