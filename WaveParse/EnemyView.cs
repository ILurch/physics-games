﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using GXPEngine.TMXParse;
using GXPEngine.Verlet;


namespace GXPEngine.WaveParse
{
    public class EnemyView : AnimationSprite
    {
        private static Bitmap[] bitmaps = new Bitmap[]
        {
            new Bitmap("Sprite_SmallFly.png"),
            new Bitmap("Sprite_BigFly.png"),
            new Bitmap("Sprite_Beetle.png")
        };
        
        private static string[] sprites = {"Sprite_SmallFly.png", "Sprite_BigFly.png", "Sprite_Beetle.png"};
        private static int[] colums = {11, 11, 11};
        private static int[] rows = {2, 2, 1};

        public enum EnemyState
        {
            APPEARING, CAUGHT, FADING, FLYING
        }

        private EnemyState _oldState;
        private EnemyState _state;
        
        private Dictionary<EnemyState,Animation> _smallAnimations = new Dictionary<EnemyState, Animation>();
        private Dictionary<EnemyState,Animation> _bigAnimations = new Dictionary<EnemyState, Animation>();
        private Dictionary<EnemyState,Animation> _beetleAnimations = new Dictionary<EnemyState, Animation>();
        private Animation _currentAnimation;

        private Dictionary<EnemyState, Animation>[] _animations;

        private Vec2 _position;
        private float _detectionRange = 10;

        private int _type;
        private int count;
        
        public bool beingEaten;
        private int eatCounter;
        
        public EnemyView(int type, Vec2 startPosition) : base((Bitmap) bitmaps[type-1], colums[type-1], rows[type-1])
        {
            _position = startPosition;
            _oldState = EnemyState.APPEARING;
            _state = EnemyState.APPEARING;
            _type = type - 1;
            InitiateAnimations();
            _currentAnimation = _animations[_type][_state];
            count = 0;
            SetPosition(_position);
            SetOrigin(width/2,height/2);
            _rotation += 90;
        }

        private void InitiateAnimations()
        {
            int[] smallAppearingArray = {0, 1, 2};
            int[] smallFadingArray = {2, 1, 0};
            int[] smallFlyingArray = {10, 9, 8, 9};
            int[] smallCaughtArray = {7,6,5,4,3,4,5,6 };
            
            _smallAnimations.Add(EnemyState.APPEARING, new Animation(smallAppearingArray,4));
            _smallAnimations.Add(EnemyState.FADING, new Animation(smallFadingArray,4));
            _smallAnimations.Add(EnemyState.FLYING, new Animation(smallFlyingArray,4));
            _smallAnimations.Add(EnemyState.CAUGHT, new Animation(smallCaughtArray,4));
            
            int[] bigAppearingArray = {0, 1, 2};
            int[] bigFadingArray = {2, 1, 0};
            int[] bigFlyingArray = {10, 9, 8, 9};
            int[] bigCaughtArray = {7,6,5,4,3,4,5,6 };
            
            _bigAnimations.Add(EnemyState.APPEARING, new Animation(bigAppearingArray,4));
            _bigAnimations.Add(EnemyState.FADING, new Animation(bigFadingArray,4));
            _bigAnimations.Add(EnemyState.FLYING, new Animation(bigFlyingArray,4));
            _bigAnimations.Add(EnemyState.CAUGHT, new Animation(bigCaughtArray,4));

            int[] beetleAppearingArray = {3,4,5 };
            int[] beetleFadingArray = {5,4,3 };
            int[] beetleFlyiingArray = {10,9,8,7,6,7,8,9 };
            
            _beetleAnimations.Add(EnemyState.APPEARING, new Animation(beetleAppearingArray,4));
            _beetleAnimations.Add(EnemyState.FADING, new Animation(beetleFadingArray,4));
            _beetleAnimations.Add(EnemyState.FLYING, new Animation(beetleFadingArray,4));

            _animations = new[] {_smallAnimations, _bigAnimations, _beetleAnimations};
        }

        Vec2 CheckRopeCollision()
        {
            foreach (VerletRope rope in MyGame.instance.getVerletManager().Ropes)
            {
                if (rope.checkColision(_position, _detectionRange) != null)
                {
                    VerletConstraint constraint = rope.checkColision(_position, _detectionRange);

                    if (constraint.Start.Position.DistanceNotSquared(_position) >
                        constraint.End.Position.DistanceNotSquared(_position))
                        return constraint.End.Position;
                    else return constraint.Start.Position;
                }
            }
            return null;
        }

        void SetPosition(Vec2 position)
        {
            x = position.x;
            y = position.y;
        }

        public Vec2 GetPosition()
        {
            return _position;
        }

        public EnemyState GetState()
        {
            return _state;
        }

        public int GetType()
        {
            return _type;
        }
        
        void Update()
        {
            if (_currentAnimation != null)
            {
                if (_oldState != _state)
                {
                    _currentAnimation.Stop();
                    _currentAnimation = _animations[_type][_state];
                }
                else
                {
                    _currentAnimation.Update();
                    SetFrame(_currentAnimation.Count);
                }
                count++;
                _oldState = _state; 
                if (count == 12)
                {
                    _state = EnemyState.FLYING;
                }
                if (count == 50)
                {
                    if (CheckRopeCollision() != null)
                    {//Collision
                        SetPosition(CheckRopeCollision());
                        _state = EnemyState.CAUGHT;
                    }
                    else
                    {//No Collision
                        _state = EnemyState.FADING;
                        
                    }
                }

                if (_state == EnemyState.FADING && count == 62)
                {
                    Destroy();
                }

                if (beingEaten)
                {
                    eatCounter++;
                    if(eatCounter == 40)
                        Destroy();
                }
            }
            
            //This should be last to update
        }
    }
}