﻿using System.IO;
using System.Xml.Serialization;
using GXPEngine.TMXParse;

namespace GXPEngine
{
    public class WaveParser
    {
        private WaveParser()
        {
        }

        public static Waves ParseWaveFile(string file)
        {
            XmlSerializer _serializer = new XmlSerializer(typeof(Waves));
            TextReader reader = new StreamReader(file);
            Waves waves = _serializer.Deserialize(reader) as Waves;
            waves.file = file;
            return waves;
        }
    }
}