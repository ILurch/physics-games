﻿using System.Collections.Generic;

namespace GXPEngine.Physics
{
    public class PhysicsManager
    {

        private List<PhysicStick> _sticks = new List<PhysicStick>();
        private Game _game;
        
        public PhysicsManager(Game game)
        {
            _game = game;
        }
        
        public void Add(PhysicStick stick)
        {
            _sticks.Add(stick);
        }

        public void AddAll(List<PhysicStick> sticks)
        {
            foreach (PhysicStick stick in sticks)
            {
                _sticks.Add(stick);
            }
        }
        
        public void Update()
        {
            foreach (PhysicStick stick in _sticks)
            {
                if (stick.EndNode.parent != null && stick.StartNode.parent != null)
                {
                    _game.OnAfterStep += stick.Update;
                }
            }
        }        
    }
}