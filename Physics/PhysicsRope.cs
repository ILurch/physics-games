﻿using System;
using System.Collections.Generic;

namespace GXPEngine.Physics
{
    public class PhysicsRope
    {
        public List<PhysicStick> Elements => _elements;
        public int ElementLength => _elementLength;

        private List<PhysicStick> _elements = new List<PhysicStick>();
        private int _elementLength;

        public PhysicsRope(GameObject parent, Vec2 pos, int elementLength, int elements)
        {
            _elementLength = elementLength;
            generate(parent, pos, elements);
        }

        private void generate(GameObject parent, Vec2 pos, int length)
        {
            for (int i = 0; i < length; i++)
            {
                PhysicStick stick;
                if (i > 0)
                {
                    PhysicStick above = _elements[i - 1];
                    stick = new PhysicStick(parent, above.EndNode, above.EndNode.Pos.Clone().Add(_elementLength, 15));
                }
                else
                {
                    stick = new PhysicStick(parent, pos.Clone(), pos.Clone().Add(_elementLength, 0));
                }
                _elements.Add(stick);
            }
        }
    }
}