﻿using System.Collections.Generic;

namespace GXPEngine.Physics
{
    public class PhysicsBox
    {
        public List<PhysicStick> Sticks => _sticks;

        private List<PhysicStick> _sticks;

        public PhysicsBox(GameObject parent, Vec2 pos, int width, int height)
        {
            _sticks = new List<PhysicStick>();
            PhysicStick top = new PhysicStick(parent, pos.Clone(), pos.Clone().Add(width, 0));
            PhysicStick bot = new PhysicStick(parent, pos.Clone().Add(0, height), pos.Clone().Add(width, height));
            PhysicStick left = new PhysicStick(parent, top.StartNode, bot.StartNode);
            PhysicStick right = new PhysicStick(parent, top.EndNode, bot.EndNode);
            _sticks.Add(top);
            _sticks.Add(bot);
            _sticks.Add(left);
            _sticks.Add(right);
        }
    }
}