﻿using System;
using System.Drawing;

namespace GXPEngine.Physics
{
    public class PhysicNode : Canvas
    {
        public bool UseGravity
        {
            get => _useGravity;
            set => _useGravity = value;
        }


        public bool Visible
        {
            get => visible;
            set => visible = value;
        }

        public Vec2 Pos
        {
            get => _position;
            set => _position = value;
        }

        public Vec2 OldPos
        {
            get => _oldPosition;
            set => _oldPosition = value;
        }

        public Vec2 Velocity
        {
            get => _velocity;
            set => _velocity = value;
        }

        public bool Pinned
        {
            get => _pinned;
            set => _pinned = value;
        }


        public Color Color { get; set; }

        private float _bounciness = 0.3f;
        private Vec2 _gravity = new Vec2(0, 0.481f);
        private bool _useGravity;

        private Vec2 _velocity = Vec2.GetZero();

        private Vec2 _oldPosition;
        private Vec2 _position;
        private int _radius;

        private bool _pinned;

        public PhysicNode(Vec2 pos, int radius) : base(radius * 2, radius * 2)
        {
            SetOrigin(width / 2f, height / 2f);
            _position = pos;
            _oldPosition = pos;
            _radius = radius;
            Color = Color.Red;
            SetXY(pos.x, pos.y);
            if (visible)
                draw();
            _useGravity = true;
        }

        public void Update()
        {
            if (_pinned) return;
            _oldPosition = _position.Clone();
            if (UseGravity)
                _velocity.Add(_gravity);
            _position.Add(_velocity);
            SetXY(_position.x, _position.y);
            float t = 0f;
            if (_position.x - _radius < Screen.LeftXBoundary)
            {
                t = (_oldPosition.x - Screen.LeftXBoundary - _radius) / (_oldPosition.x - _position.x);
                _position.x = Screen.LeftXBoundary + _radius;
                _position.y -= _velocity.y * (1 - t);
                _velocity.x *= -_bounciness;
            }
            else if (_position.x + _radius > Screen.RightXBoundary)
            {
                t = (Screen.RightXBoundary - _oldPosition.x - _radius) / (_position.x - _oldPosition.x);
                _position.x = Screen.RightXBoundary - _radius;
                _position.y -= _velocity.y * (1 - t);
                _velocity.x *= -_bounciness;
            }
            if (_position.y - _radius < Screen.TopYBoundary)
            {
                t = (_oldPosition.y - Screen.TopYBoundary - _radius) / (_oldPosition.y - _position.y);
                _position.y = Screen.TopYBoundary + _radius;
                _position.x -= _velocity.x * (1 - t);
                _velocity.y *= -_bounciness;
            }
            else if (_position.y + _radius > Screen.BottomYBoundary)
            {
                t = (Screen.BottomYBoundary - _oldPosition.y - _radius) / (_position.y - _oldPosition.y);
                _position.y = Screen.BottomYBoundary - _radius;
                _position.x -= _velocity.x * (1 - t);
                _velocity.y *= -_bounciness;
            }
        }

        public float DistanceTo(PhysicNode other)
        {
            Vec2 d = Pos.Clone() - other.Pos.Clone();
            return d.Length();
        }

        private void draw()
        {
            graphics.FillEllipse(new SolidBrush(Color), 0, 0, width, height);
        }
    }
}