﻿using System;
using System.Runtime.Serialization.Formatters;

namespace GXPEngine.Physics
{
    public class PhysicStick
    {
        public float Length => _startNode.DistanceTo(_endNode);

        public PhysicNode StartNode => _startNode;
        public PhysicNode EndNode => _endNode;

        private PhysicNode _startNode;
        private PhysicNode _endNode;

        private GameObject _parent;

        private LineSegment _line;
        private float _length;

        public PhysicStick(GameObject parent, Vec2 start, Vec2 end)
        {
            _parent = parent;
            _startNode = new PhysicNode(start, 5);
            _endNode = new PhysicNode(end, 5);
            initialize();
        }

        public PhysicStick(GameObject parent, PhysicNode start, Vec2 end)
        {
            _parent = parent;
            _startNode = start;
            _endNode = new PhysicNode(end, 5);
            initialize();
        }

        public PhysicStick(GameObject parent, PhysicNode start, PhysicNode end)
        {
            _parent = parent;
            _startNode = start;
            _endNode = end;
            initialize();
        }

        private void initialize()
        {
            _line = new LineSegment(_startNode.Pos, _endNode.Pos);
            if (_parent != null)
            {
                if (!_parent.HasChild(_startNode))
                    _parent.AddChild(_startNode);
                if (!_parent.HasChild(_endNode))
                    _parent.AddChild(_endNode);
                _parent.AddChild(_line);
            }
            _length = _startNode.DistanceTo(_endNode);
        }

        public void Update()
        {
            Vec2 offset = _endNode.Pos.Clone().Subtract(_startNode.Pos);
            float distance = _startNode.DistanceTo(_endNode);
            float diff = _length - distance;
            float percent = diff / distance / 2;
            offset.Scale(percent);
            if (!_startNode.Pinned)
            {
                _startNode.Pos.Subtract(offset);
                _line.start.SetXY(_startNode.Pos);
                _startNode.SetXY(_startNode.Pos.x, _startNode.y);
            }
            if (!_endNode.Pinned)
            {
                _endNode.Pos.Add(offset);
                _line.end.SetXY(_endNode.Pos);
                _endNode.SetXY(_endNode.Pos.x, _endNode.y);
            }
        }
    }
}