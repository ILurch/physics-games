﻿using System;
using System.Drawing.Drawing2D;

namespace GXPEngine
{
    public class MainMenu : Sprite
    {
        private Sound _mainMusic;
        public Sound _hover;
        private Sound _scrollMenu;
        public SoundChannel _backgroundMusicChannel;

        private Sprite _text;
        private AnimationSprite _highlight;
        
        private int _selection = 0;

        public MainMenu() : base("Menu_Background.png")
        {
            _text = new Sprite("Menu_Text.png");
            _highlight = new AnimationSprite("Menu_Selection.png",3,2);
            InitializeManu();
        }

        void InitializeManu()
        {
            _mainMusic = new Sound("music2.mp3", false, false);
            _backgroundMusicChannel = _mainMusic.Play();
            _scrollMenu = new Sound("main.mp3");
            _hover = new Sound("mainMenuClick.ogg");
            
            AddChild(_text);
            AddChild(_highlight);
            _highlight.SetXY(167,305);
        }

        public void Update()
        {
            if (Input.GetKeyDown(Key.UP) || Input.GetKeyDown(Key.W))
            {
                _selection--;
            }

            if (Input.GetKeyDown(Key.DOWN) || Input.GetKeyDown(Key.S))
            {
                _selection++;
            }
            if (_selection > 3)
                _selection = 0;
            if (_selection < 0)
                _selection = 3;

            _highlight.SetFrame(_selection);

            if (Input.GetKeyDown(Key.SPACE) || Input.GetKeyDown(Key.ENTER))
            {
                switch (_selection)
                {
                    case  0:
                        StartGame(0);
                        break;
                    case 1:
                        StartGame(1);
                        break;
                    case 2:
                        //CreditsMenu();
                        break;
                    case 3:
                        ExitGame();
                        break; 
                }
            }
        }

        private void StartGame(int level)
        {
            MyGame.instance.StartGame(level);
            _backgroundMusicChannel.Stop();
            Destroy();
            _hover.Play();
        }

        public void ControlsMenu()
        {
            _hover.Play();
            Controls controls = new Controls(this);

            parent.AddChild(controls);
        }

        public void CreditsMenu()
        {
            _hover.Play();
            Credits credits = new Credits(this);
            parent.AddChild(credits);
        }

        public void ExitGame()
        {
            parent.Destroy();
        }
    }
}