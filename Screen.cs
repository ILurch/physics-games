﻿using System;

namespace GXPEngine
{
    public class Screen : Canvas
    {
        /*
            Used for Screen overlay effects, etc
        */

        public static int LeftXBoundary;
        public static int RightXBoundary;
        public static int TopYBoundary;
        public static int BottomYBoundary;

        public HUD HUD => _hud;

        private MyGame _game;

        private Popup _popup;
        private AnimationSprite _ballon;

        private HUD _hud;

        public Screen(MyGame game, int width, int height) : base(width, height)
        {
            _game = game;
            _ballon = new AnimationSprite("ballon256.png", 11, 1);
            _game.AddChild(_ballon);
            _ballon.SetXY(5, height - _ballon.height - 5);
            _hud = new HUD();
            AddChild(_hud);
        }

        public void SetBorder()
        {
            int border = 0;
            LeftXBoundary = border;
            RightXBoundary = (_game.GetMapManager().CurrentMap.Map.width * 32) - border - LeftXBoundary;
            TopYBoundary = 0;
            BottomYBoundary = (_game.GetMapManager().CurrentMap.Map.height * 32) - border - TopYBoundary;
        }

        public void Update()
        {
            if (_game.GetMapManager().CurrentMap == null) return;
            x = -1 * _game.GetPlayer().GetPosition().x + game.width / 2f + _game.GetPlayer().width / 2f;
            y = -1 * _game.GetPlayer().GetPosition().y + game.height / 2f + _game.GetPlayer().height / 2f;

            if (y > 0)
                y = 0;
            if (x > 0)
                x = 0;
            if (x < -1 * (_game.GetMapManager().CurrentMap.Map.width * 32 - game.width))
                x = -1 * (_game.GetMapManager().CurrentMap.Map.width * 32 - game.width);
            if (y < -1 * (_game.GetMapManager().CurrentMap.Map.height * 32 - game.height))
                y = -1 * (_game.GetMapManager().CurrentMap.Map.height * 32 - game.height);

            Player player = MyGame.instance.GetPlayer();

            _hud.SetXY(-x, -y);
            _hud.SetScore(player.Score);
            _hud.SetWaveCount(MyGame.instance.GetMapManager().GetWaveManager().WaveCount + 1);
            SetChildIndex(_hud, int.MaxValue);
        }

        public void showBallon(int stay)
        {
            _popup.visible = true;
            SetChildIndex(_popup, 1000);
            _popup.Start(stay);
        }
    }
}