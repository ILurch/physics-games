﻿using System;
using System.Drawing;
using System.Security;
using GXPEngine.Physics;
using GXPEngine.Verlet;

namespace GXPEngine
{
    public class RopeCreator
    {
        public VerletConstraint LastElement => _above;
        public Vec2 Start => _start.Clone();
        public PhysicNode Projectile => _projectile;
        public VerletRope Rope => _rope;
        public bool Destroyed => _destroyed;

        private Player.StringType _type;
        private Player _player;
        
        private VerletRope _rope;
        private Vec2 _oldPos;
        private PhysicNode _projectile;

        private VerletConstraint _above;

        private bool _destroyed;

        private Vec2 _start;

        public RopeCreator(Vec2 start, PhysicNode projectile, Player.StringType type, Player player)
        {
            _player = player;
            _start = start;
            _type = type;
            _rope = new VerletRope(start, 25);
            MyGame.instance.getVerletManager().Add(_rope);
            _projectile = projectile;
            _oldPos = _projectile.Pos;
            
        }

        public void Update()
        {
            if (_destroyed)
            {
                if (_projectile != null)
                {
                    _projectile.visible = false;
                    _projectile = null;
                }
                return;
            }


            Vec2 direction = _above != null
                ? _projectile.Pos.Clone() - _above.End.Position
                : _projectile.Pos.Clone() - _start;

            for (int i = 1; i < Int32.MaxValue; i++)
            {
                if (direction.Length() > i * _rope.ElementLength)
                {
                    addElement(direction);
                    MyGame.instance.GetPlayer().totalPieces++;
                }
                else
                {
                    break;
                }
            }
        }

        public VerletConstraint checkCollision()
        {
            if (LastElement != null)
            {
                foreach (VerletRope rope in MyGame.instance.getVerletManager().Ropes)
                {
                    if (rope.Equals(_rope)) continue;

                    VerletConstraint constraint = rope.checkColision(LastElement.End.Position, 3);

                    if (constraint != null)
                    {
                        return constraint;
                    }
                }
            }
            return null;
        }

        private void addElement(Vec2 direction)
        {
            _above = _rope.AddElement(_projectile.parent, direction, _type);
            _above.Line.color = MyGame.ToUint(Color.FromArgb(20, Color.Bisque));
            if (MyGame.instance.GetPlayer().GetGoo() == 0)
            {
                Destroy();
                _rope.Release(null);
            }
        }

        public void Destroy()
        {
            _destroyed = true;
        }
    }
}