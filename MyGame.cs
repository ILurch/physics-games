using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq.Expressions;
using GXPEngine;
using GXPEngine.Verlet;
using GXPEngine.WaveParse;

public class MyGame : Game, WaveManager.WaveListener
{
    public static MyGame instance;

    public static readonly int WIDTH = 1920;
    public static readonly int HEIGHT = 1080;

    public static uint ToUint(Color c)
    {
        return (uint) (((c.A << 24) | (c.R << 16) | (c.G << 8) | c.B) & 0xffffffffL);
    }

    static void Main()
    {
        instance = new MyGame();
        instance.Start();
    }

    private Player _player;

    private VerletManager _verlet;

    private MapManager _maps;
    private Screen _screen;

    private MainMenu _mainMenu;
    private GameOver _gameOver;

    public MyGame() : base(WIDTH, HEIGHT, true)
    {
        init();
    }

    private void init()
    {
        _gameOver = new GameOver();
        _screen = new Screen(this, width, height);
        _mainMenu = new MainMenu();
        AddChild(_mainMenu);
        AddChild(_gameOver);
        _gameOver.Hide();
        _maps = new MapManager();
        _verlet = new VerletManager();
    }

    public void StartGame(int level)
    {
        _mainMenu.Destroy();
        SetLevel(level);
    }

    public void SetLevel(int level)
    {
        Level l = GetMapManager().CurrentMap;
        if (l != null)
            l.Destroy();
        l = _maps.InitializeLevel(_screen, level);
        _screen.SetBorder();
        _player = new Player(_maps.CurrentMap.FindPlayerSpawn());
        l.AddChild(_player);
        AddChild(_screen);
        GetMapManager().GetWaveManager().SetListener(this);
        Cursor cursor = new Cursor();
        AddChild(cursor);
    }

    public Screen GetScreen()
    {
        return _screen;
    }

    public MapManager GetMapManager()
    {
        return _maps;
    }

    public Player GetPlayer()
    {
        return _player;
    }

    public VerletManager getVerletManager()
    {
        return _verlet;
    }

    public void Update()
    {
        //_physics.Update();
        _verlet.Update();
        checkGameOver();
    }

    private void checkGameOver()
    {
        if (_gameOver.visible)
        {
            if (Input.GetKeyDown(Key.SPACE))
            {
                //showMenu();
                SetLevel(1);
                _gameOver.visible = false;
            }
        }
    }

    private void showMenu()
    {
        foreach (GameObject child in GetChildren().ToArray())
        {
            child.Destroy();
        }
        init();
    }
    
    public void onGameOver()
    {
        GetPlayer().Destroy();
        _gameOver.SetXY((width / 2f)  - (_gameOver.width / 2f),(height / 2f)  - (_gameOver.height / 2f));
        _gameOver.Show();
        SetChildIndex(_gameOver, 10000);
    }
}